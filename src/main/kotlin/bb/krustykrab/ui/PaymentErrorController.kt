package bb.krustykrab.ui

import bb.krustykrab.constant.ProcessVariables.ACTION
import bb.krustykrab.service.UIMediator
import javax.faces.view.ViewScoped
import javax.inject.Named

@ViewScoped
@Named
class PaymentErrorController(
        private val uiMediator: UIMediator
) {

    fun toPayment() {
        uiMediator.completeTask(mapOf(ACTION to "toPayment"))
    }

    fun cancelOrder() {
        uiMediator.completeTask(mapOf(ACTION to "cancelOrder"))
    }
}