package bb.krustykrab.ui

import bb.krustykrab.constant.ProcessVariables
import bb.krustykrab.model.OrderItem
import bb.krustykrab.service.UIMediator
import javax.faces.view.ViewScoped
import javax.inject.Named

@ViewScoped
@Named
class OrderController(
        private val uiMediator: UIMediator
) {

    var orderItems: MutableList<OrderItem> = mutableListOf()

    var burgerTypes = listOf(
            "Krabby Patty",
            "Double Krabby Patty",
            "Junior Small Patty"
    )

    fun addOrderItem() {
        orderItems.add(OrderItem())
    }

    fun confirmOrder() {
        uiMediator.completeTask(
                mapOf(
                        ProcessVariables.ORDER_ITEMS to orderItems,
                        ProcessVariables.ACTION to "confirmOrder"
                )
        )
    }

    fun cancelOrder() {
        uiMediator.completeTask(mapOf(ProcessVariables.ACTION to "cancelOrder"))
    }

    fun exit() {
        uiMediator.openTaskList()
    }
}