package bb.krustykrab.ui

import bb.krustykrab.service.UIMediator
import javax.faces.view.ViewScoped
import javax.inject.Named

@ViewScoped
@Named
class CookingController(
        private val uiMediator: UIMediator
) {

    fun ready() {
        uiMediator.completeTask()
    }
}