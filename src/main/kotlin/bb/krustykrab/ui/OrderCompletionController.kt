package bb.krustykrab.ui

import bb.krustykrab.service.UIMediator
import javax.faces.view.ViewScoped
import javax.inject.Named

@ViewScoped
@Named
class OrderCompletionController(
        private val uiMediator: UIMediator
) {

    fun orderCompleted() {
        uiMediator.completeTask()
    }
}