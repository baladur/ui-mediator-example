package bb.krustykrab.ui

import bb.krustykrab.constant.ProcessVariables.ACTION
import bb.krustykrab.service.UIMediator
import javax.faces.view.ViewScoped
import javax.inject.Named

@ViewScoped
@Named
class PaymentController(
        private val uiMediator: UIMediator
) {

    fun payedByCash() {
        uiMediator.completeTask(mapOf(ACTION to "payedByCash"))
    }

    fun payByCard() {
        uiMediator.completeTask(mapOf(ACTION to "payByCard"))
    }

    fun back() {
        uiMediator.completeTask(mapOf(ACTION to "back"))
    }

    fun exit() {
        uiMediator.openTaskList()
    }
}