package bb.krustykrab.constant

object ProcessVariables {
    val ACTION = "action"
    val ORDER_ITEMS = "orderItems"
    val ORDER_TOTAL_COST = "orderTotalCost"
}