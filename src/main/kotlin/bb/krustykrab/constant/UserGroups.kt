package bb.krustykrab.constant

object UserGroups {
    val CAMUNDA_ADMIN = "camunda-admin"
    val CASHIER_GROUP = "cashier-group"
    val COOK_GROUP = "cook-group"
}