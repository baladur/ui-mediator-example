package bb.krustykrab.app

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication(exclude = [(RepositoryRestMvcAutoConfiguration::class)])
@ComponentScan(basePackages = ["bb.krustykrab"])
@EnableProcessApplication
class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
        }
    }
}