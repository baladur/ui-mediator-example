package bb.krustykrab.service

import org.camunda.bpm.engine.TaskService
import org.camunda.bpm.engine.task.Task
import org.springframework.stereotype.Service
import org.springframework.web.context.annotation.SessionScope

interface UIMediator {

    /**
     * Save taskId and callbackUrl, find processInstanceId by taskId. If given task is not assigned to current user,
     * redirect to callbackUrl.
     */
    fun startTask()

    /**
     * Complete current user task with given variables. If openTaskList is true, redirect to callbackUrl,
     * otherwise find next permitted user task and redirect to its formKey.
     */
    fun completeTask(variables: Map<String, Any?> = mapOf(), openTaskList: Boolean = false)

    /**
     * Redirect to task list by callbackUrl.
     */
    fun openTaskList()
}

@Service("uiMediator")
@SessionScope
class UIMediatorImpl(
        private val taskService: TaskService,
        private val userTaskFormService: UserTaskFormService
) : UIMediator {

    private lateinit var processInstanceId: String

    private lateinit var taskId: String

    private lateinit var callbackUrl: String

    var processVariables: MutableMap<String, Any?> = mutableMapOf()

    private fun openNextForm() {
        val nextTask = userTaskFormService.findAndClaimTask(processInstanceID = processInstanceId)
        val nextUrl = nextTask?.let(::encodeTaskURL) ?: callbackUrl
        FacesUtils.redirect(nextUrl)
    }

    override fun openTaskList() = FacesUtils.redirect(callbackUrl)

    override fun startTask() {
        val taskParams = FacesUtils.getTaskParams()
        val taskId = taskParams["taskId"]
        val callbackUrl = taskParams["callbackUrl"]
        if (taskId != null && callbackUrl != null) {
            when (userTaskFormService.openTask(taskId)) {
                is ClaimingResult.Success -> {
                    this.taskId = taskId
                    this.processInstanceId = userTaskFormService.getProcessIDForTask(taskId)
                    this.processVariables = taskService.getVariables(taskId)
                    this.callbackUrl = callbackUrl
                }
                is ClaimingResult.Error,
                is ClaimingResult.NoPermissionsError -> FacesUtils.redirect(callbackUrl)
            }
        }
    }

    override fun completeTask(variables: Map<String, Any?>, openTaskList: Boolean) {
        userTaskFormService.complete(taskId, this.processVariables + variables)
        if (openTaskList) FacesUtils.redirect(callbackUrl)
        else openNextForm()
    }

    private fun encodeTaskURL(task: Task): String {
        val taskIDRequestParam = "taskId=${task.id}"
        val callbackURLParam = "callbackUrl=$callbackUrl"
        return "${task.formKey}?$taskIDRequestParam&$callbackURLParam"
    }
}