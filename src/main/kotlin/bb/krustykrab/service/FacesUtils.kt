package bb.krustykrab.service

import javax.faces.context.FacesContext

object FacesUtils {
    fun getTaskParams(): Map<String, String> =
            FacesContext.getCurrentInstance().externalContext.requestParameterMap

    fun redirect(url: String) = FacesContext.getCurrentInstance().externalContext.redirect(url)
}