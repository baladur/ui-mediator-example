package bb.krustykrab.service

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import org.camunda.bpm.engine.AuthorizationException
import org.camunda.bpm.engine.HistoryService
import org.camunda.bpm.engine.IdentityService
import org.camunda.bpm.engine.ProcessEngineException
import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.TaskService
import org.camunda.bpm.engine.task.Task
import org.springframework.stereotype.Service
import java.io.Serializable


interface UserTaskFormService {
    /**
     * Find next permitted user task in the process and claim it
     */
    fun findAndClaimTask(processInstanceID: String): Task?

    /**
     * Claim user task and return its form key
     */
    fun claimTask(taskID: String): ClaimingResult

    /**
     * Check whether task belongs to current user
     */
    fun openTask(taskID: String): ClaimingResult

    /**
     * Return process ID for task with taskID
     */
    fun getProcessIDForTask(taskID: String): String

    fun complete(taskId: String, variables: Map<String, Any?> = mapOf())
}

@Service("userTaskFormService")
class UserTaskFormServiceImpl(
        private val taskService: TaskService,
        private val identityService: IdentityService,
        private val runtimeService: RuntimeService,
        private val historyService: HistoryService
) : UserTaskFormService {
    var pollingTime: Long = 200L
    var searchFormTimeout: Long = 60000L

    override fun findAndClaimTask(processInstanceID: String): Task? {
        return waitForTask(processInstanceID)?.let { task ->
            if (!isTaskPermittedForCurrentUser(task.id)) return null
            when (claimTask(task.id)) {
                is ClaimingResult.Success -> task

                is ClaimingResult.NoPermissionsError,
                is ClaimingResult.Error -> null
            }
        }
    }

    override fun claimTask(taskID: String): ClaimingResult {
        return try {
            taskService.claim(taskID, identityService.currentAuthentication.userId)
            ClaimingResult.Success(taskService.createTaskQuery().taskId(taskID).initializeFormKeys().singleResult().formKey)
        } catch (e: ProcessEngineException) {
            ClaimingResult.Error
        } catch (e: AuthorizationException) {
            ClaimingResult.NoPermissionsError
        }
    }

    override fun openTask(taskID: String): ClaimingResult {
        return try {
            val task = taskService.createTaskQuery().taskId(taskID).active().initializeFormKeys().singleResult()
            if (task.assignee == identityService.currentAuthentication.userId) {
                ClaimingResult.Success(task.formKey)
            } else ClaimingResult.Error
        } catch (e: ProcessEngineException) {
            ClaimingResult.Error
        }
    }

    override fun getProcessIDForTask(taskID: String): String {
        return taskService.createTaskQuery().taskId(taskID).list().first().processInstanceId
    }

    private fun waitForTask(processInstanceID: String) = runBlocking {
        withTimeoutOrNull(searchFormTimeout) {
            var task: Task? = null
            while (task == null) {
                if (isProcessCompleted(processInstanceID)) {
                    return@withTimeoutOrNull null
                }
                task = findTask(processInstanceID)
                if (task == null) {
                    delay(pollingTime)
                }
            }
            task
        }
    }

    private fun isProcessCompleted(processInstanceID: String?): Boolean =
            historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(processInstanceID)
                    .completed().count().toInt() == 1

    private fun findTask(processInstanceId: String): Task? =
            taskService.createTaskQuery().initializeFormKeys()
                    .processInstanceId(processInstanceId)
                    .list().firstOrNull()

    private fun isTaskPermittedForCurrentUser(taskId: String): Boolean =
            taskService.createTaskQuery().taskId(taskId).taskCandidateGroupIn(identityService.currentAuthentication.groupIds).count().toInt() == 1

    override fun complete(taskId: String, variables: Map<String, Any?>) {
        taskService.complete(taskId, variables)
    }
}

sealed class ClaimingResult {
    object Error : ClaimingResult()
    object NoPermissionsError : ClaimingResult()
    data class Success(val formKey: String) : ClaimingResult()
}