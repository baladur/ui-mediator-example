package bb.krustykrab.service

import bb.krustykrab.model.OrderItem
import org.springframework.stereotype.Service

@Service
class OrderCostService {
    fun calculateTotalCost(orderItems: List<OrderItem>): Double =
            orderItems.foldRight(0.00) { orderItem, totalCost ->
                totalCost + (orderItem.quantity ?: 1) * priceList[orderItem.burgerType]!!
            }

    private val priceList = mapOf(
            "Krabby Patty" to 1.00,
            "Double Krabby Patty" to 2.00,
            "Junior Small Patty" to 0.70
    )
}