package bb.krustykrab.model

import java.io.Serializable

data class OrderItem(
        var burgerType: String? = null,
        var quantity: Int? = null
) : Serializable