package bb.krustykrab.delegate

import bb.krustykrab.constant.ProcessVariables
import bb.krustykrab.model.OrderItem
import bb.krustykrab.service.OrderCostService
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component

@Component("OrderCostDelegate")
class OrderCostDelegate(
        private val orderCostService: OrderCostService
) : JavaDelegate {
    override fun execute(execution: DelegateExecution) {
        val orderItems = execution.getVariable(ProcessVariables.ORDER_ITEMS) as List<OrderItem>
        val orderCost = orderCostService.calculateTotalCost(orderItems)
        execution.setVariable(ProcessVariables.ORDER_TOTAL_COST, orderCost)
    }
}