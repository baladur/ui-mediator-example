package bb.krustykrab.delegate

import bb.krustykrab.constant.ProcessVariables
import org.camunda.bpm.engine.delegate.BpmnError
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component

@Component("CardPaymentDelegate")
class CardPaymentDelegate : JavaDelegate {
    override fun execute(execution: DelegateExecution) {
        // оплата не проходит за 1 миллисекунду!
        Thread.sleep(5000)

        val paymentAmount = execution.getVariable(ProcessVariables.ORDER_TOTAL_COST) as Double
        if (paymentAmount < 2.00) {
            throw BpmnError("666", "Слишком маленькая сумма для оплаты. Сквидвард, научись уже продавать крабсбургеры! (Мистер Крабс)")
        }
    }
}