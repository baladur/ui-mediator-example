package bb.krustykrab.configuration

import org.camunda.bpm.engine.ProcessEngine
import org.camunda.bpm.engine.authorization.Authorization
import org.camunda.bpm.engine.authorization.Authorization.AUTH_TYPE_GRANT
import org.camunda.bpm.engine.authorization.Permissions
import org.camunda.bpm.engine.authorization.Resources
import org.camunda.bpm.engine.identity.Group
import org.camunda.bpm.engine.spring.SpringProcessEnginePlugin
import org.springframework.context.annotation.Configuration
import bb.krustykrab.constant.UserGroups.CAMUNDA_ADMIN
import bb.krustykrab.constant.UserGroups.COOK_GROUP
import bb.krustykrab.constant.UserGroups.CASHIER_GROUP

@Configuration
class SpringBootProcessEnginePlugin : SpringProcessEnginePlugin() {
    override fun postProcessEngineBuild(processEngine: ProcessEngine) {
        super.postProcessEngineBuild(processEngine)
        with(processEngine) {
            val camundaAdmin = addGroup(CAMUNDA_ADMIN)
            val cashierGroup = addGroup(CASHIER_GROUP)
            val cookGroup = addGroup(COOK_GROUP)

            addGroupPermissions(cashierGroup.id)
            addGroupPermissions(cookGroup.id)

            createFilter("Задачи кассира", cashierGroup.id)
            createFilter("Задачи повара", cookGroup.id)

            createUser("admin", "Eugene", "Krabs", camundaAdmin.id)
            createUser("squidward", "Squidward", "Tentacle", cashierGroup.id)
            createUser("spongebob", "Sponge Bob", "Square Pants", cookGroup.id)
        }
    }

    private fun ProcessEngine.existingGroup(groupID: String): Group? {
        return identityService.createGroupQuery().groupId(groupID).singleResult()
    }

    private fun ProcessEngine.addGroup(groupID: String): Group {
        return existingGroup(groupID) ?: identityService.newGroup(groupID).apply {
            identityService.saveGroup(this)
        }
    }

    private fun ProcessEngine.existingGroupAuth(group: String, resource: Resources, resourceId: String): Authorization? {
        return authorizationService.createAuthorizationQuery().groupIdIn(group).resourceType(resource)
                .resourceId(resourceId).singleResult()
    }

    private fun ProcessEngine.addGroupAuth(
            authType: Int, group: String, resource: Resources, resourceId: String, vararg
            permissions: Permissions
    ) {
        existingGroupAuth(group, resource, resourceId) ?: authorizationService.saveAuthorization(authorizationService
                .createNewAuthorization(authType).apply {
                    groupId = group
                    setResource(resource)
                    this.resourceId = resourceId
                    permissions.forEach { addPermission(it) }
                })
    }

    private fun ProcessEngine.createUser(userID: String, firstName: String, lastName: String, groupID: String) {
        if (identityService.createUserQuery().userId(userID).count().toInt() == 0) {
            val user = identityService.newUser(userID)
                    .apply {
                        password = "123"
                        this.firstName = firstName
                        this.lastName = lastName
                        identityService.saveUser(this)
                    }

            identityService.createMembership(user.id, groupID)
        }
    }

    private fun ProcessEngine.existingFilter(filterName: String) =
            filterService.createFilterQuery().filterName(filterName).singleResult()

    private fun ProcessEngine.createFilter(filterName: String, groupID: String) {
        val filter = existingFilter(filterName)
                ?: filterService.newTaskFilter(filterName)
                        .setQuery(
                                taskService.createTaskQuery()
                                        .active()
                                        .taskCandidateGroup(groupID)
                                        .includeAssignedTasks()
                        ).let(filterService::saveFilter)
        addGroupAuth(
                AUTH_TYPE_GRANT, groupID, Resources.FILTER, filter.id,
                Permissions.ALL
        )
    }

    private fun ProcessEngine.addGroupPermissions(groupID: String) {
        addGroupAuth(AUTH_TYPE_GRANT, groupID, Resources.APPLICATION, "tasklist",
                Permissions.ACCESS)
        addGroupAuth(
                AUTH_TYPE_GRANT, groupID, Resources.PROCESS_DEFINITION, "*",
                Permissions.CREATE_INSTANCE,
                Permissions.READ,
                Permissions.READ_INSTANCE,
                Permissions.READ_HISTORY,
                Permissions.UPDATE_INSTANCE,
                Permissions.UPDATE
        )
        addGroupAuth(
                AUTH_TYPE_GRANT, groupID, Resources.PROCESS_INSTANCE, "*",
                Permissions.CREATE,
                Permissions.READ,
                Permissions.UPDATE_INSTANCE,
                Permissions.UPDATE
        )
        addGroupAuth(
                AUTH_TYPE_GRANT, groupID, Resources.TASK, "*",
                Permissions.READ,
                Permissions.UPDATE
        )
        addGroupAuth(
                AUTH_TYPE_GRANT, groupID, Resources.GROUP, CASHIER_GROUP,
                Permissions.READ
        )
    }
}