package bb.krustykrab.configuration

import org.camunda.bpm.engine.impl.plugin.AdministratorAuthorizationPlugin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CamundaConfig {
    @Bean
    fun administratorAuthorizationPlugin() =
            AdministratorAuthorizationPlugin().apply {
                administratorUserName = "admin"
            }
}