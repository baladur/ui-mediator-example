package bb.krustykrab.configuration

import com.mchange.v2.c3p0.ComboPooledDataSource
import org.h2.tools.Server
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.DependsOn

@Configuration
class DBConfig {
    @Bean("camundaDataSource")
    @ConfigurationProperties(prefix = "camunda.datasource")
    @DependsOn("h2Server")
    fun camundaDataSource(): ComboPooledDataSource = ComboPooledDataSource()

    @Bean(initMethod = "start", destroyMethod = "stop")
    fun h2Server(): Server = Server.createTcpServer(
            "-baseDir", "./build", "-tcp", "-tcpAllowOthers", "-tcpPort", "9095")
}