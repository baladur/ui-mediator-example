package bb.krustykrab.configuration

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class SecurityConfig : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http
                .authorizeRequests()
                .antMatchers(
                        "/actuator/**",
                        "/api/**",
                        "/app/**",
                        "/view/**",
                        "/lib/**",
                        "/webjars/**",
                        "/javax.faces.resource/**",
                        "/resources/**",
                        "/error/**",
                        "/favicon.ico",
                        "/"
                ).permitAll()
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().csrf().disable()
    }
}