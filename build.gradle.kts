import org.gradle.internal.impldep.org.junit.experimental.categories.Categories.CategoryFilter.exclude
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "bb.krustykrab"
version = "1.0-SNAPSHOT"

val kotlinxCoroutinesVersion = "1.0.1"
val camundaSpringBootVersion = "3.0.0"
val camundaVersion = "7.9.0"

plugins {
    val kotlinVersion = "1.3.31"
    val springBootVersion = "2.0.8.RELEASE"
    val springDependencyMgr = "1.0.6.RELEASE"

    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
    kotlin("plugin.jpa") version kotlinVersion
    id("org.springframework.boot") version springBootVersion
    id("io.spring.dependency-management") version springDependencyMgr
    id("org.sonarqube") version "2.7"
    groovy
    jacoco
}

dependencyManagement {
    imports {
        mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf("-Xjsr305=strict")
    }
}

repositories {
    mavenCentral()
}

dependencies {
    // Common
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")

    // Spring
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    // Camunda
    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-webapp:$camundaSpringBootVersion")
    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-rest:$camundaSpringBootVersion")
    implementation("org.camunda.bpm:camunda-engine:$camundaVersion")
//    implementation("org.camunda.bpm:camunda-engine-cdi:$camundaVersion")

    // DB
    implementation("com.h2database:h2:1.4.196")
    implementation("org.hibernate:hibernate-c3p0")

    // UI
    implementation("org.primefaces:primefaces:6.2")
    implementation("org.primefaces.extensions:primefaces-extensions:6.2")

    implementation("org.joinfaces:jsf-spring-boot-starter:3.2.4") {
        exclude(group = "org.primefaces", module = "primefaces")
    }
    implementation("javax.enterprise:cdi-api:2.0.SP1")
    implementation("org.glassfish:javax.faces:2.4.0")

    implementation(fileTree("lib") { include("*.jar") })
}